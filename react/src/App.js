import React, {Component} from 'react';
import Row from 'react-bootstrap/Row';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container';
import AssignJob from './jobAllocation/assignJob'
import AddUser from './authentication/addUser'
import EmployeeView from './authentication/employeeView'
import DeviceView from './device/viewDevice'
import JobAllocation from './jobAllocation/jobAllocation';
import viewJob from './jobAllocation/viewJob';
import Navigation from './component/nav';
import viewRefill from './refill/viewRefill';
import HomeView from './home/homeView';
import Login from './authentication/login';
import logout from './authentication/logout';
import { connect } from 'react-redux';
import UserNav from './component/userNav';
import WeightConvView from './weightConversion/weightConvView';

class App extends Component 
{
	render() {
		let route  = (<Route exact path = '/' component = {Login}/>)
		let nav = <UserNav/>
		if(localStorage.getItem('userId') == 2) {

			route = <>
						<Route exact path = '/addEmployee' component = {AddUser}/>
						<Route exact path = '/jobAllocation' component = {JobAllocation}/>
						<Route exact path = '/viewEmployee' component = {EmployeeView}/>
						<Route exact path = '/device' component = {DeviceView}/>
						<Route exact path = '/refill' component = {viewRefill}/>
						<Route exact path = '/viewJob/:id/:name' component = {viewJob}/>
						<Route exact path = '/assignJob/:id/:name' component = {AssignJob}/>
						<Route exact path = '/weightConversion' component = {WeightConvView}/>
						<Route exact path = '/' component = {HomeView}/>
						<Route exact path = '/logout' component = {logout}/>
					</>
			nav = <Navigation/>
		}
			return (
				<React.Fragment>
					{nav}
				<Container fluid>		
					<Row>
						<Col>
							<Router>
								<switch>
									{route}
								</switch>
							</Router>
  						</Col>
					</Row>
  				</Container>
 	
				</React.Fragment>
			)
		}
}

const mapStateToProps = state => {
	return {
		role : state.auth.userId
	}
}

export default connect(mapStateToProps, null)(App) 


