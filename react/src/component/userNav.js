import React, {Component} from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'



class UserNav extends Component 
{
  render() {
    return (
      <Navbar collapseOnSelect expand = "lg" bg = "dark" variant = "dark">
        <Navbar.Toggle />
        <Navbar.Collapse >
          <Nav className = "mr-auto">
            
          </Nav>
          <Nav>
          
            <Nav.Link href = "http://localhost:3000"> Login </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar> 
    );
  }
}

export default UserNav