import React, {Component} from 'react';
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import { connect } from 'react-redux';
import {  NavDropdown } from 'react-bootstrap';

class Navigation extends Component 
{
  render() {
    
    return (
      <Navbar collapseOnSelect expand = "lg" bg = "dark" variant = "dark">
        <Navbar.Toggle />
        <Navbar.Collapse >
          <Nav className = "mr-auto">
          <Nav.Link href="http://localhost:3000"> Home </Nav.Link>
          <Nav.Link href="http://localhost:3000/device"> Device </Nav.Link>
          <Nav.Link href="http://localhost:3000/jobAllocation"> Job Allocation </Nav.Link>
          <Nav.Link href="http://localhost:3000/refill"> Refill </Nav.Link>
          <Nav.Link href="http://localhost:3000/weightConversion"> Weight conversion </Nav.Link>
            <NavDropdown title = "Employee">
              <NavDropdown.Item href = "http://localhost:3000/addEmployee"> Add employee </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="http://localhost:3000/viewEmployee"> View / Edit </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Nav>
            
              
               <Nav.Link href= {"http://localhost:3000/logout"}> Logout </Nav.Link>
            
        
          </Nav>
        </Navbar.Collapse>
      </Navbar> 
    );
  }
}
const mapStateToProps = state => {
  return {
      auth : state.auth.userId
  }
}


export default connect(mapStateToProps, null)(Navigation)
