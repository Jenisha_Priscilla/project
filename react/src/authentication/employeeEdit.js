import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/employee'
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faCheck } from "@fortawesome/free-solid-svg-icons";

class EmployeeEdit extends Component 
{
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            emailError : '',
            fieldError : ''
        }
    }

    componentDidMount() {
        this.props.getEmployee(this.props.id);
    }

    validate() {
        var fieldError = " ";
        var emailError = " ";
        
        if (this.props.employee.user_rfid == '' ||this.props.employee.user_email== '') {
            fieldError = 'All fields must be filled';
        }
        if (fieldError === " ") {
            var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
            if(!this.props.employee.user_email.match(pattern)){
                emailError = 'Enter valid mail id';
            }
           
        }
        if (fieldError != " "  ) {
            this.setState({fieldError, emailError})
            return false;
        }
        return true;
    }
   
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const obj = {
                id:this.props.id,
                rf_id:this.props.employee.user_rfid,
                user_email:this.props.employee.user_email
            };

            console.log(obj)
            axios.post('http://localhost:5000/php/employee/updateEmployee.php',obj)
            .then(response => {
                  console.log(response.data)
               
                    alert('Data edited successfully');
                   this.props.onEdit()
                   this.props.employeeUpdateSuccess();
                
            })
            .catch(err => console.log(err))
        }
    }
    render() {
        console.log("edit")
        return (
            <tr >
                <td>           
                   {this.props.id}
                </td>
                  
                <td > 
                    {this.props.employee.user_name}
                <div style ={{ fontSize:10, color:"red"}}>
                    {this.state.fieldError}
                </div>     
                </td>
                <td >
                    <Form.Control type = "text" name = "user_rfid"
                        value = {this.props.employee.user_rfid}
                        onChange = {this.props.onEmployeeChange}
                    />
                </td>
                <td >
                    <Form.Control type = "text" name = "user_email"
                        value = {this.props.employee.user_email}
                        onChange = {this.props.onEmployeeChange}
                    />
                </td>
                <td >
                    <Form.Control type = "text" name = "user_pwd"
                        value = {this.props.employee.user_pwd}
                        onChange = {this.props.onEmployeeChange}
                    />
                </td>
                <td > 
                    <div variant="dark" onClick = {this.onSubmit} > 
                        <FontAwesomeIcon icon = {faCheck} size = "sm" />
                    </div>
                </td>
            </tr>      
            )
        }  
    }
    const mapStateToProps = state => {
        return {
            employee : state.employee.employee,
        }
    }
    
    const mapDispatchToProps = dispatch => {
        return {
            getEmployee : (employeeId) => dispatch(actionTypes.getEmployee(employeeId)), 
            onEmployeeChange : (e) => dispatch(actionTypes.handleEmployeeEdit(e)),
            employeeUpdateSuccess : () => dispatch(actionTypes.employeeUpdateSuccess())
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeEdit) 