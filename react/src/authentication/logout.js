import React, {Component} from 'react';
import {Redirect} from 'react-router'
import * as actionTypes from '../store/actions/auth'
import { connect } from 'react-redux';
class Logout extends Component 
{

    
  componentDidMount() {
    console.log("Logout");
    localStorage.setItem('userId',' ');
    console.log(localStorage.getItem ('userId'))  

    this.props.authLogout()
  }

  render() {
    if (localStorage.getItem ('userId')) {
      
      return (<Redirect to={'/'}/>)
    
    }
    
  }
}
const mapDispatchToProps = dispatch => {
  return {
      authLogout : () => dispatch(actionTypes.authLogout()), 
  }
}

export default connect(null, mapDispatchToProps)(Logout)