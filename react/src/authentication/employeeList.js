import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faTrash} from "@fortawesome/free-solid-svg-icons";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import EmployeeEdit from './employeeEdit';
import * as actionTypes from '../store/actions/employee'
class EmployeeList extends Component
{
    constructor(props) {
        super(props);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.state = {
            redirect: false,
            delete : false
        }
    }
    
    edit() {
        this.setState ({
            redirect : !this.state.redirect
        })
        console.log('onedit')
        this.props.onUpdate();
    }
    
    delete() {
        axios.get('http://localhost:5000/php/employee/deleteEmployee.php?id='+this.props.obj.user_id)
        .then(() => {
            this.setState({ redirect: false, delete: true})
        })
        .catch(err => console.log(err))
    }
    
    render() {
        console.log(this.state.redirect)
        let result = ( <tr>
                        <td>
                            {this.props.obj.user_id}
                        </td>
                        <td>
                            {this.props.obj.user_name}
                        </td>
                        <td>
                            {this.props.obj.user_rfid}
                        </td>
                        <td>
                            {this.props.obj.user_email}
                        </td>
                        
                        { this.props.update == 1  ?
                             <td style = {{width: '2px'}}>
                             <div onClick =  {()=> this.edit()}>  
                                 <FontAwesomeIcon icon = {faPen} size = "sm" />
                             </div>
                             </td> :null
                        }
                        <td  style = {{width: '2px'}}>
                            <div variant = "danger" onClick = {this.delete} > 
                                <FontAwesomeIcon icon = {faTrash} size = "sm" /> 
                            </div>
                        </td>
                    </tr> )
                
       
        if (this.state.redirect && !this.props.updateSuccess ) {
            result =  <EmployeeEdit id = {this.props.obj.user_id}  onEdit = {this.edit}/>
        }
       
        if (this.state.delete) {
            result =  <Redirect to={'/employeeView'}/>
        }
        return result;
        
    }
}

const mapStateToProps = state => {
    return {
        update: state.employee.update,
        updateSuccess : state.employee.updateSuccess
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdate : () => dispatch(actionTypes.updateEmployee())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeList)



