import React, {Component} from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import emailjs from 'emailjs-com'
import Card from 'react-bootstrap/Card'
import axios from 'axios'

class AddUser extends Component 
{
    constructor(props) {
        super(props);
        this.state = {
            sender:'jenishabriskela@gmail.com',
            employeeName : '',
            mailId: '',
            fieldError : '',
            role: '',
            RFID :'',
            userId: 'user_vUAYR1NW80rNcjgphZsgU',   
            templateId: 'mail',  
            employeeNameError : '',
            emailError : ''         
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange =this.handleChange.bind(this);
        this.onCheck = this.onCheck.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate() {
        
        var fieldError = " ";
        var emailError = " ";
        var employeeNameError = " ";
        
        if (this.state.employeeName == '' || this.state.mailId == '' || this.state.role == '' || this.state.RFID == '' )  {
            fieldError = 'All the fields must be filled';
        }
        if(fieldError === " ") {
            var patt1 = /^[a-zA-Z ]+$/g; 
            if (!this.state.employeeName.match(patt1)) {
                employeeNameError = 'Must contain only alphabets';
            }
            var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
            if(!this.state.mailId.match(pattern)){
                emailError = 'Enter valid mail id';
            }
            
           
        }

        if (fieldError != " " || employeeNameError != " " || emailError != " " ) {
            this.setState({fieldError, employeeNameError, emailError})
            return false;
        }

        return true;
    }

    onSubmit(event) {
        event.preventDefault();
        console.log(this.state)
        const isValid= this.validate();
        if (isValid) {     
            const { templateId } = this.state;
            const password = Math.random().toString(36).slice(-8);
            const obj = {
                employeeName : this.state.employeeName,   
                role :this.state.role,
                RFID :this.state.RFID,
                mailId : this.state.mailId,
                password : password
            }
                this.sendFeedback(
                    templateId,
                    this.state.sender,
                    this.state.mailId,
                    password
                );
            this.setState({   employeeName : '',
                                mailId: '',
                                role: '',
                                RFID :''
                        })
            axios.post('http://localhost:5000/php/employee/addUser.php',obj)
            .then(response => {
               console.log(response.data)
                })
            .catch(err => console.log(err))
        }
        
       
    }
    
    sendFeedback(templateId, senderEmail, receiverEmail, feedback) {
        emailjs.send('gmail', templateId, {
                senderEmail,
                receiverEmail,
                feedback
        },this.state.userId)
        .then(() => {
            console.log('MAIL SENT!')
            alert("Mail Sent")
        })
        .catch(err => console.error('Failed to send feedback. Error: ', err));
    }

    onCheck(e){
        if(e.target.checked) {
            console.log(e.target.value)
            this.setState({gender : e.target.value})
            
        }  
        if(!e.target.checked) {
            this.setState({gender : ''})
        }    
    }

    handleChange(e) {
        this.setState({[e.target.name]:e.target.value})
    }

    render() {
        
        return (
            <Container>
                
             
                <Row>
                
                    <Col md={{ span: 7, offset: 3 }}>
               
                        <Card style ={{marginTop:50}}> 
                       
                        <Card.Body>
                        <div style ={{marginTop:20}}>
                            <Form onChange = {this.handleChange}>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.fieldError}
                                </div>   
                               
                                                 
                            <Form.Row>
                                <Col sm ="4">
                                    <Form.Label>Employee Name</Form.Label>
                                </Col>
                                <Col>
                                    <Form.Control type ="text" placeholder="Employee Name " 
                                    name = "employeeName" value = {this.state.employeeName}
                                     />
                                </Col>
                            </Form.Row>
                            <div style = {{ fontSize:12, color:"red"}}>
                                {this.state.employeeNameError}
                            </div>  
                              
                            
                            <br/> 
                            <Form.Row>
                                <Col sm ="4">
                                    <Form.Label> Role </Form.Label>
                                </Col>
                                <Col>
                                    <Form.Control as = "select" name = "role"   value = {this.state.role}>
                                        <option value ="" disabled selected > Role </option>
                                            <option>Employee</option>
                                            <option>Supervisor</option>
                                    </Form.Control>
                                </Col>
                            </Form.Row>
                            <br/>  

                             <Form.Row>
                                <Col sm ="4">
                                    <Form.Label>RF id</Form.Label>
                                </Col>
                                <Col>
                                    <Form.Control type ="text" placeholder= "RF id" name = "RFID"
                                    value = {this.state.RFID} />
                                </Col>
                            </Form.Row>
                            <br/>          

                            <Form.Row>
                                <Col sm ="4">
                                    <Form.Label> Mail Id</Form.Label>
                                </Col>
                                <Col>
                                    <Form.Control type ="email" placeholder="E-mail id" 
                                    name ="mailId" value = {this.state.mailId}/>
                                </Col>
                            </Form.Row>
                            <div style = {{ fontSize:12, color:"red"}}>
                                {this.state.emailError}
                            </div> 
                                
                            <Button variant="dark" onClick = {this.onSubmit} > Add user </Button>
                        </Form>
                    </div>
                    </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        );
    }
}

export default AddUser;