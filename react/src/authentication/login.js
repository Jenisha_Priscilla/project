import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/auth'

class Login extends Component 
{
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            userNameError: '',
            fieldError: ''
        };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.state.username && this.state.password) {
            this.setState ({fieldError:""})
            const obj= {
                Name:this.state.username,
                Password:this.state.password
            };
            axios.post('http://localhost:5000/php/authentication/login.php',obj)
            .then(response => {
             
                if (response.data == 1 || response.data == 2 || response.data == 3) {
                    localStorage.setItem('userId',response.data);
                    this.props.authSuccess(response.data);
                    console.log(response.data)
                }
                else {
                    this.setState({ userNameError: "Type username or password correctly" })
                }
            })
            .catch(err => console.log(err))
        } 
        else {
        this.setState({ fieldError: "Must fill all the feilds" })
        } 
    }

    onChangePassword(e) {
        this.setState({ password:e.target.value })
    }
  
    onChangeUserName(e) {
        this.setState({ username:e.target.value })
    }

    render() {
        return (
        <Container>
            <Row>
                <Col md={{ span: 7, offset: 3 }}>
                    <div style ={{marginTop:50}}>
                        <Form>
                            <Form.Group >
                                <Form.Label>User Name:</Form.Label>
                                <Form.Control type ="text" placeholder="Username"
                                onChange ={this.onChangeUserName} />
                            </Form.Group>
                
                            <Form.Group >
                                <Form.Label> Password:</Form.Label>
                                <Form.Control type ="password" placeholder="Password"
                                onChange ={this.onChangePassword}/>
                            </Form.Group>

                            <div style = {{fontSize:12, color:"red"}}>
                                {this.state.userNameError}
                            </div> 

                            <div style ={{ fontSize:12, color:"red"}}>
                                {this.state.fieldError}
                            </div> 
            
                            <Button variant = "dark" onClick = {this.onSubmit} > Login </Button>
                        </Form>

                    </div>
                </Col>
            </Row>
        </Container>
        ); 
    }
}

const mapDispatchToProps = dispatch => {
    return {
        authSuccess : (userId) => dispatch(actionTypes.authSuccess(userId)), 
    }
}

export default connect(null , mapDispatchToProps)(Login)