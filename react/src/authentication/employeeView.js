import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import EmployeeList from './employeeList'
import { connect } from 'react-redux';

const EmployeeView  = (props)=> {

    const [employeeData, setEmployeeData] = useState([])
    useEffect(() =>{
        axios.get('http://localhost:5000/php/employee/viewEmployee.php')
        .then(response => {
            setEmployeeData(response.data)
        })
        .catch (function(error) {
            console.log(error);
        })
    }, [ props.updateSuccess])
    
   const employeeList = () => {
        return employeeData.map (function(object, i) {
            return <EmployeeList obj = {object} key ={i} />;        
      });
    }
        return (
            
         <div style = {{marginTop : '20px', marginLeft : '100px', marginRight: '100px'}}>
                  
                    <Table responsive hover>
                        <thead>
                            <tr>
                                <th> User id</th>
                                <th> Name </th>
                                <th> RF id</th>
                                <th> Mail id</th>
                                <th colSpan = "2"> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {employeeList()}
                        </tbody>
                    </Table> 
             
                </div>
        )
    
}
const mapStateToProps = state => {

    return {
      
        updateSuccess : state.employee.updateSuccess
    }
}
export default connect(mapStateToProps, null)(EmployeeView)

