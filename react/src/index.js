import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import  {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware, compose ,combineReducers} from 'redux'
import employeeReducer from './store/reducers/employee'
import deviceReducer from './store/reducers/device'
import viewJobReducer from './store/reducers/viewJob'
import assignJobReducer from './store/reducers/assignJob'
import refillReducer from './store/reducers/refill'
import authReducer from './store/reducers/auth'
const rootReducer = combineReducers ({
 employee: employeeReducer,
 device : deviceReducer,
 viewJob : viewJobReducer,
 assignJob : assignJobReducer,
 refill : refillReducer,
 auth : authReducer
}); 

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
));

const app = (
  <Provider store = {store}>
      <App/>
  </Provider>
)

ReactDOM.render(
  app,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
