import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/refill'
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faCheck } from "@fortawesome/free-solid-svg-icons";

class RefillEdit extends Component 
{
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            fieldError : ''
        }
    }

    componentDidMount() {
        console.log(this.props.id)
        this.props.getRefill(this.props.id);
        this.props.getUser()
    }

    validate() {
        var fieldError = " ";
       
        
        if (this.props.refill.refill_weight == '' || this.props.refill.user_name == '' ) {
            fieldError = 'All fields must be filled';
        }
        if (fieldError != " "  ) {
            this.setState({fieldError})
            return false;
        }
        return true;
    }
   
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const obj = {
                refill_weight : this.props.refill.refill_weight,
                user_name : this.props.refill.user_name,
                device_id : this.props.refill.device_id
            };

            console.log(obj)
            axios.post('http://localhost:5000/php/refill/updateRefill.php',obj)
            .then(response => {
                  console.log(response.data)
               
                    alert('Data edited successfully');
                   this.props.onEdit()
                   this.props.refillUpdateSuccess();
                
            })
            .catch(err => console.log(err))
        }
    }
    render() {
        console.log("edit")
        return (
            <tr >    
                <td>
                    {this.props.refill.device_id}
                </td>
                <td>
                    {this.props.refill.device_name}
                </td>
                <td >
                <Form.Control as = "select" name = "user_name"
                   > 
                  <option >{this.props.refill.user_name} </option>
                  {this.props.user.map (function(object, i) {  
                      return  <option > {object.user_name}</option>; 
                    })}
               </Form.Control>
                </td>
                <td >
                    <Form.Control type = "text" name = "refill_weight"
                        value = {this.props.refill.refill_weight}
                        onChange = {this.props.onRefillChange}
                    />
                </td>
                <td >
                    {this.props.refill.refill_updatedon === null ? this.props.refill.refill_createdon
                        : this.props.refill.refill_updatedon}
                </td>
                <td>
                    {this.props.refill.raw_weight}
                </td>
                <td>
                {
                    this.props.refill.refill_status == null ? '' :  
                        this.props.refill.refill_status == 0 ? 'Pending' : 'Success' 
                }
                </td>
               
                <td > 
                    <div variant="dark" onClick = {this.onSubmit} > 
                        <FontAwesomeIcon icon = {faCheck} size = "sm" />
                    </div>
                </td>
            </tr>      
            )
        }  
    }
    const mapStateToProps = state => {
        return {
            refill: state.refill.refill,
            user : state.refill.user
        }
    }
    
    const mapDispatchToProps = dispatch => {
        return {
            getRefill: (refillId) => dispatch(actionTypes.getRefill(refillId)), 
            onRefillChange : (e) => dispatch(actionTypes.handleRefillEdit(e)),
            refillUpdateSuccess : () => dispatch(actionTypes.refillUpdateSuccess()),
            getUser : () => dispatch(actionTypes.getUser())
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)(RefillEdit) 