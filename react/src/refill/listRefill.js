import React, { PureComponent } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux';
import RefillInsert from './insertRefill'
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import RefillEdit from './editRefill';
import * as actionTypes from '../store/actions/refill'
class RefillList extends PureComponent
{
    constructor(props) {
        super(props);
        this.edit = this.edit.bind(this);
        this.add = this.add.bind(this)
        this.state = {
            redirect: false,
            insert : false
        }
    }
    edit() {
        this.setState ({
            redirect : !this.state.redirect
        })
        console.log('onedit')
        this.props.onUpdate();
    }
    add() {
        this.setState ({
            redirect : !this.state.redirect, insert : true
        })
        console.log('onInsert')
        this.props.onInsert();
    }
    
  
    
    render() {
        console.log(this.props)
        let result = ( <tr>
                        <td>
                            {this.props.obj.device_id}
                        </td>
                        <td>
                            {this.props.obj.device_name}
                        </td>
                        <td >
                            {this.props.obj.user_name}
                        </td>
                        <td >
                            {this.props.obj.refill_weight}
                        </td>
                        <td >
                            {this.props.obj.refill_updatedon === null ? this.props.obj.refill_createdon
                                : this.props.obj.refill_updatedon}
                        </td>
                        <td>
                            {this.props.obj.raw_weight}
                        </td>
                        <td>
                            {
                                this.props.obj.refill_status == null ? '' :  
                                    this.props.obj.refill_status == 0 ? 'Pending' : 'Success' 
                            }
                            
                        </td>
                        {this.props.obj.user_name !== null ?
                           (  this.props.update == 1  ?
                             <td style = {{width: '2px'}}>
                             <div onClick =  {()=> this.edit()}>  
                                 <FontAwesomeIcon icon = {faPen} size = "sm" />
                             </div>
                             </td> :null)
                        :
                        (this.props.update == 1  ?
                             <td style = {{width: '2px'}}>
                             <div onClick =  {()=> this.add()}>  
                                 <FontAwesomeIcon icon = {faPlus} size = "sm" />
                             </div>
                             </td> :null)
                        
                    }
                    </tr> )
                
       
        if (this.state.redirect && !this.props.updateSuccess ) {
            result =  <RefillEdit id = {this.props.obj.device_id}  onEdit = {this.edit}/>
        }
        if (this.state.redirect && !this.props.insertSuccess && this.state.insert ) {
            result =  <RefillInsert id = {this.props.obj.device_id}  onAdd = {this.add}/>
        }
       
       
        return result;
        
    }
}

const mapStateToProps = state => {
    return {
        update: state.refill.update,
        updateSuccess : state.refill.updateSuccess,
        insertSuccess : state.refill.insertSuccess
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdate : () => dispatch(actionTypes.updateRefill()),
        onInsert : () => dispatch(actionTypes.insertRefill())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RefillList)



