import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/refill'
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faCheck } from "@fortawesome/free-solid-svg-icons";

class RefillInsert extends Component 
{
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            fieldError : '',
            device : '',
            user : []
        }
    }

    componentDidMount() {
        this.props.getRefill(this.props.id);
        axios.post('http://localhost:5000/php/refill/getDevice.php/?id='+ this.props.id)
        .then(response => {
            console.log(response.data)
            this.setState({device : response.data})
        })
        .catch(err => console.log(err))
        this.props.getUser()
        
    }

    validate() {
        var fieldError = " ";
       
        
        if (this.props.refill.refill_weight == '' || this.props.refill.user_name == '' ) {
            fieldError = 'All fields must be filled';
        }
        if (fieldError != " "  ) {
            this.setState({fieldError})
            return false;
        }
        return true;
    }
   
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const obj = {
                refill_weight : this.props.refill.refill_weight,
                user_name: this.props.refill.user_name,
                device_id : this.props.id
            };

            console.log(obj)
            axios.post('http://localhost:5000/php/refill/insertRefill.php',obj)
            .then(response => {
                  console.log(response.data)
                  
                    alert('Data added successfully');
                   this.props.onAdd()
                  this.props.refillInsertSuccess()
                   
                
            })
            .catch(err => console.log(err))
        }
    }
    render() {
       
        return (
            <tr >    
                <td>
                    {this.props.id}
                </td>
                <td>
                    {this.state.device}
                </td>
                <td >
                <Form.Control as = "select" name = "user_name"
                  placeholder="User name"   onChange = {this.props.onRefillChange}>
                  <option value = "" disabled selected> User name </option>
                  {this.props.user.map (function(object, i) {  
                      return  <option value = {object.user_id}> {object.user_name}</option>; 
                    })}
               </Form.Control>
                    {/* <Form.Control type = "text" name = "user_name"
                        onChange = {this.props.onRefillChange}
                    /> */}
                </td>
                <td >
                    <Form.Control type = "text" name = "refill_weight"
                        onChange = {this.props.onRefillChange}
                    />
                </td>
                <td colSpan = '3'>
                   {this.props.refill.refill_status}
                </td>
                
                <td > 
                    <div variant="dark" onClick = {this.onSubmit} > 
                        <FontAwesomeIcon icon = {faCheck} size = "sm" />
                    </div>
                </td>
            </tr>      
            )
        }  
    }
    const mapStateToProps = state => {
        return {
            refill: state.refill.refill,
            user : state.refill.user
        }
    }
    
    const mapDispatchToProps = dispatch => {
        return {
            getRefill: (refillId) => dispatch(actionTypes.getRefill(refillId)), 
            onRefillChange : (e) => dispatch(actionTypes.handleRefillEdit(e)),
            refillInsertSuccess : () => dispatch(actionTypes.refillInsertSuccess()),
            getUser : () => dispatch(actionTypes.getUser())
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)(RefillInsert) 