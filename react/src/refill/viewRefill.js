import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import RefillList from './listRefill'
import { connect } from 'react-redux';

const RefillView  = (props)=> {

    const [refillData, setRefillData] = useState([])
    useEffect(() =>{
        axios.get('http://localhost:5000/php/refill/viewRefill.php')
        .then(response => {
            console.log(response.data)
            setRefillData(response.data)
        })
        .catch (function(error) {
            console.log(error);
        })
    }, [ props.updateSuccess, props.insertSuccess])
    
   const refillList = () => {
        return refillData.map (function(object, i) {
            return <RefillList obj = {object} key ={i} />;        
      });
    }
    const style = {
        width: '95vm'
    }
        return (
            
         <div style = {{marginTop: '20px'}}>
                   
                    <Table responsive striped bordered hover style = {style}>
                        <thead>
                            <tr>
                                <th> Id</th>
                                <th> Device name </th>
                                <th> User name</th>
                                <th> Refill weight</th>
                                <th> Last refill at</th>
                                <th style = {{width : '150px'}}> Ava. weight</th>
                                <th> Status</th>
                                <th > Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {refillList()}
                        </tbody>
                    </Table> 
             
                </div>
        )
    
}
const mapStateToProps = state => {

    return {
        insertSuccess : state.refill.insertSuccess,
        updateSuccess : state.refill.updateSuccess
    }
}
export default connect(mapStateToProps, null)(RefillView)

