import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import HomeList from './homeList'

const HomeView = (props)=> {

    const [data, setData] = useState([])
    useEffect(() =>{
        axios.get('http://localhost:5000/php/home/home.php')
        .then(response => {
            console.log(response.data)
            setData(response.data)
        })
        .catch (function(error) {
            console.log(error);
        })
    }, [])
    
   const homeList = () => {
        return data.map (function(object, i) {
            return <HomeList obj = {object} key ={i} />;        
      });
    }
    const style = {
        width: '95vm'
    }
        return (
            
         <div style = {{marginTop: '20px'}}>
                   
                    <Table responsive striped bordered hover style = {style}>
                    <thead>
                        <tr>
                            
                            <th colSpan ="3" onClick = {()=> props.history.push('/device')}><center> Device</center> </th>
                            <th colSpan ="3" onClick = {()=> props.history.push('/jobAllocation')}> <center> Job allocation </center></th>
                            <th colSpan ="3" onClick = {()=> props.history.push('/refill')}><center> Refill </center></th>
                            <th>Buzzer</th>
                           
                        </tr>
                            <tr>
                                <th> Id</th>
                                <th> Device name </th>
                                <th> Raw weight </th>
                                <th> Employee id</th>
                                <th> Created on</th>
                                <th> Status</th>
                                <th> Employee id</th>
                                <th> Created on</th>
                                <th> Status</th>
                                <th > Buzz status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {homeList()}
                        </tbody>
                    </Table> 
             
                </div>
        )
    
}

export default HomeView

