import React, { PureComponent } from 'react'


class HomeList extends PureComponent
{

    status(status) {
        if(status == 0 ) {
            return <div style = {{color : 'red'}}> Pending </div>
        } else {
            return <div style = {{color : 'green'}}> Success</div>
        }
    }
    render() {
 
        let result = ( <tr>
                        <td>
                            {this.props.obj.device_id}
                        </td>
                        <td>
                            {this.props.obj.device_name}
                        </td>
                        <td >
                            {this.props.obj.raw_weight}
                        </td>
                        <td >
                            {this.props.obj.job_user}
                        </td>
                        <td>
                            {this.props.obj.job_createdon}
                        </td>
                        <td>
                            {
                                this.props.obj.job_status == null ? null:  this.status(this.props.obj.job_status)
                            }
                         
                        </td>
                        <td >
                            {this.props.obj.refill_user}
                        </td>
                        <td >
                            {this.props.obj.refill_createdon}
                        </td>
                        <td>
                            {
                                this.props.obj.refill_status == null ? null:  this.status(this.props.obj.job_status)
                            }
                            
                        </td>
                        <td>
                            {this.props.obj.buzz_status}
                        </td>
                        
                    </tr> )

        return result;
        
    }
}

export default HomeList



