import * as actionTypes from './actionTypes'
import axios from 'axios';

export const getEmployee = (userId) => {
    return dispatch => {
        axios.get('http://localhost:5000/php/employee/getEmployee.php/?id='+userId)
        .then(response => {
            const employee ={...response.data['0']}
            dispatch(setEmployee(employee))
        })
        .catch(err => console.log(err))
    }
   
}

export const setEmployee = (employee) => {
    return {
        type: actionTypes.SET_EMPLOYEE,
        employee : employee
    }
}

// export const handleStationChange = (event) => {
//     return {
//         type: actionTypes.HANDLE_STATIONCHANGE,
//         event : event
//     }
// };

export const handleEmployeeEdit = (event) => {
    return {
        type: actionTypes.HANDLE_CHANGE,
        event : event
    }
};

export const updateEmployee = () => {
    return {
        type: actionTypes.EMPLOYEE_UPDATE
    }
}

export const employeeUpdateSuccess = () => { 
    return {
        type: actionTypes.UPDATE_SUCCESS
    }
}

// export const routeInsertSuccess = () => {
//     return {
//         type : actionTypes.ROUTE_INSERT_SUCCESS
//     }
// }






