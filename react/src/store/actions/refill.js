import * as actionTypes from './actionTypes'
import axios from 'axios';

export const getRefill = (refillId) => {
    return dispatch => {
        axios.get('http://localhost:5000/php/refill/getRefill.php/?id='+refillId)
        .then(response => {
            const refill ={...response.data['0']}
            dispatch(setRefill(refill))
        })
        .catch(err => console.log(err))
    }  
}

export const getUser = () => {
    return dispatch => {
        axios.get('http://localhost:5000/php/refill/getUser.php')
        .then(response => {
              console.log(response.data)  
              dispatch(setUser(response.data))              
        })
        .catch(err => console.log(err))
    }
}

export const setUser = (user) => {
    return {
        type : actionTypes.SET_USER,
        user : user
    }
}

export const setRefill = (refill) => {
    return {
        type: actionTypes.SET_REFILL,
        refill : refill
    }
}


export const handleRefillEdit = (event) => {
    return {
        type: actionTypes.HANDLE_REFILL_CHANGE,
        event : event
    }
};

export const updateRefill = () => {
    return {
        type: actionTypes.REFILL_UPDATE
    }
}

export const insertRefill = () => {
    return {
        type: actionTypes.REFILL_INSERT
    }
}

export const refillUpdateSuccess = () => { 
    return {
        type: actionTypes.UPDATE_SUCCESS
    }
}

export const refillInsertSuccess = () => {
    return {
        type : actionTypes.INSERT_SUCCESS
    }
}







