import * as actionTypes from './actionTypes'
export const authSuccess = (userId) => {
    return {
        type : actionTypes.AUTH_SUCCESS,
        userId : userId
    }
}

export const authLogout = () => {
    localStorage.clear();
    return {
        type : actionTypes.AUTH_LOGOUT
    }
}