import * as actionTypes from './actionTypes'

export const hideAssignJob = () => {
    return {
        type : actionTypes.HIDE_ASSIGN_JOB
    }
}
export const showAssignJob = () => {
    return {
        type : actionTypes.SHOW_ASSIGN_JOB
    }
}

