import * as actionTypes from './actionTypes'

export const hideViewJob = () => {
    return {
        type : actionTypes.HIDE_VIEW_JOB
    }
}
export const showViewJob = () => {
    return {
        type : actionTypes.SHOW_VIEW_JOB
    }
}

export const countViewJob = () => {
    return {
        type : actionTypes.COUNT_VIEW_JOB
    }
}