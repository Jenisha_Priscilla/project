import * as actionTypes from './actionTypes'
import axios from 'axios';

export const getDevice = (deviceId) => {
    return dispatch => {
        axios.get('http://localhost:5000/php/device/getDevice.php/?id='+deviceId)
        .then(response => {
            const device ={...response.data['0']}
            dispatch(setDevice(device))
        })
        .catch(err => console.log(err))
    }
   
}

export const setDevice = (device) => {
    return {
        type: actionTypes.SET_DEVICE,
        device : device
    }
}

// export const handleStationChange = (event) => {
//     return {
//         type: actionTypes.HANDLE_STATIONCHANGE,
//         event : event
//     }
// };

export const handleDeviceEdit = (event) => {
    return {
        type: actionTypes.HANDLE_CHANGE,
        event : event
    }
};

export const updateDevice = () => {
    return {
        type: actionTypes.DEVICE_UPDATE
    }
}

export const deviceUpdateSuccess = () => { 
    return {
        type: actionTypes.UPDATE_SUCCESS
    }
}

// export const routeInsertSuccess = () => {
//     return {
//         type : actionTypes.ROUTE_INSERT_SUCCESS
//     }
// }






