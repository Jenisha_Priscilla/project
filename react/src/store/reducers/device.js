import * as actionTypes from '../actions/actionTypes'

const initialState = {
    device: {},
    insertSuccess :false,
    update : 1,
    updateSuccess : false,
    redirect : false
}

const deviceReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SET_DEVICE:
            return {
                ...state,
                device : action.device
            }

        case actionTypes.HANDLE_CHANGE :
            action.event.persist();
            const updatedDevice = {
                ...state.device
            }
            const updatedDeviceElement = {
                ...updatedDevice[action.event.target.name]
            }
          
            updatedDeviceElement.value = action.event.target.value;
            updatedDevice[action.event.target.name] = updatedDeviceElement.value 
            return {
                ...state,
                device : updatedDevice
            }

            
        case actionTypes.DEVICE_UPDATE:
            
            return{
                ...state,
                update : state.update +1,
                updateSuccess : false
            }

        case actionTypes.UPDATE_SUCCESS:
            return {
                ...state,
                update : state.update-2,
                updateSuccess : true
            }
        
        default : 
            return state;
    }
}

export default deviceReducer