import * as actionTypes from '../actions/actionTypes'

const initialState = {
    refill: {},
    insertSuccess :false,
    update : 1,
    updateSuccess : false,
    redirect : false,
    user : []
}

const refillReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SET_REFILL:
            return {
                ...state,
                refill : action.refill
            }

        case actionTypes.HANDLE_REFILL_CHANGE :
            action.event.persist();
            const updatedRefill = {
                ...state.refill
            }
            const updatedRefillElement = {
                ...updatedRefill[action.event.target.name]
            }
          
            updatedRefillElement.value = action.event.target.value;
            updatedRefill[action.event.target.name] = updatedRefillElement.value 
            return {
                ...state,
                refill : updatedRefill
            }

            
        case actionTypes.REFILL_UPDATE:
            
            return{
                ...state,
                update : state.update +1,
                updateSuccess : false
            }

        case actionTypes.REFILL_INSERT:
            
            return{
                ...state,
                update : state.update +1,
                insertSuccess : false
            }

        case actionTypes.UPDATE_SUCCESS:
            return {
                ...state,
                update : state.update-2,
                updateSuccess : true
            }
        case actionTypes.INSERT_SUCCESS:
            return {
                ...state,
                update : state.update-2,
                insertSuccess : true
            }
        case actionTypes.SET_USER:
            return{
                ...state,
                user : action.user
            }
        
        default : 
            return state;
    }
}

export default refillReducer