import * as actionTypes from '../actions/actionTypes'

const initialState = {
    show : true,
    
}

const assignJobReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.HIDE_ASSIGN_JOB:
            return {
                show: false
            }
        case actionTypes.SHOW_ASSIGN_JOB:
            return {
                show: true
            }
        default : 
            return state;
    }
}

export default assignJobReducer