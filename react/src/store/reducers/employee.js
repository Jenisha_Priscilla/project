import * as actionTypes from '../actions/actionTypes'

const initialState = {
    employee: {},
    insertSuccess :false,
    update : 1,
    updateSuccess : false,
    redirect : false
}

const employeeReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SET_EMPLOYEE:
            return {
                ...state,
                employee : action.employee
            }

        case actionTypes.HANDLE_CHANGE :
            action.event.persist();
            const updatedEmployee = {
                ...state.employee
            }
            const updatedEmployeeElement = {
                ...updatedEmployee[action.event.target.name]
            }
          
            updatedEmployeeElement.value = action.event.target.value;
            updatedEmployee[action.event.target.name] = updatedEmployeeElement.value 
            return {
                ...state,
                employee : updatedEmployee
            }

            
        case actionTypes.EMPLOYEE_UPDATE:
            
            return{
                ...state,
                update : state.update +1,
                updateSuccess : false
            }

        case actionTypes.UPDATE_SUCCESS:
            return {
                ...state,
                update : state.update-2,
                updateSuccess : true
            }
        
        default : 
            return state;
    }
}

export default employeeReducer