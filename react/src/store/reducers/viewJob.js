import * as actionTypes from '../actions/actionTypes'

const initialState = {
    show : true,
    count : 0
}

const viewJobReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.HIDE_VIEW_JOB:
            return {
                show: false
            }
        case actionTypes.SHOW_VIEW_JOB:
            return {
                show: true
            }
        case actionTypes.COUNT_VIEW_JOB:
            return {
                count : state.count+1
            }
    
        default : 
            return state;
    }
}

export default viewJobReducer