import Modal from 'react-bootstrap/Modal'
import React, {Component} from 'react';
import AssignJobForm from './assignJobForm';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/assignJob'

class AssignJob extends Component 
{
  constructor(props) {
    super(props)
    this.state = {
      show: true
    }
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.setState({ show: false});
    this.props.hideAssignJob()
    window.location.reload()
  }


render(){
    return (
        <>
          <Modal
            size="lg"
            show = {this.state.show && this.props.show}
            onHide = {this.handleClose}
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-modal-sizes-title-lg">
                Employee Name {' '}:  {this.props.name} {' '} {'('+this.props.id+ ')'}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body><AssignJobForm id = {this.props.id}/></Modal.Body>
          </Modal>
        </>
      );
    }
}
const mapStateToProps = state => {
  return {
      show : state.assignJob.show,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    hideAssignJob  : () => dispatch(actionTypes.hideAssignJob()), 
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AssignJob) 

