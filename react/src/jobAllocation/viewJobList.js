import React, { Component } from 'react'

class ViewJobList extends Component
{
    
    render() {
     
        let result = ( <tr>
                        <td>
                            {this.props.obj.device_name}
                        </td>
                        <td>
                            {this.props.obj.weight_allocated}
                        </td>
                        <td>
                            {this.props.obj.weight_taken}
                        </td>
                        {
                            this.props.obj.weight_allocated === this.props.obj.weight_taken ? 
                                <td>
                                    Completed
                                </td> : 
                                <td>
                                    Pending
                                </td>
                        }                        
                    </tr> )
                
      
        return result;
        
    }
}

export default ViewJobList



