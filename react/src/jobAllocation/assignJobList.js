import React, { PureComponent } from 'react';
import Form from 'react-bootstrap/Form'
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import Button from 'react-bootstrap/Button'
import axios from 'axios';

class AssignJobList extends PureComponent 
{
  constructor(props) {
    super(props);
    this.state = {
      device : []
    }
  }
  componentDidMount() {
    axios.get('http://localhost:5000/php/jobAllocation/getDevice.php')
    .then(response => {
        this.setState({device : response.data})
        console.log(response.data)
    })
  }

  render() {
    return (
      this.props.taskList.map((val, idx) => {
        return (
          <>
            <tr>
              <td style ={{width :'300px'}}> 
              <Form.Control as = "select" name = "Name"
                  placeholder="Device Name" data-id = {idx}>
                  <option value = "" disabled selected> Device Name </option>
                  {this.state.device.map (function(object, i) {  
                      return  <option value = {object.device_id}> {object.device_name}</option>; 
                    })}
               </Form.Control>
              </td>
              <td style ={{width :'180px'}}>
                <Form.Control 
                  type="number" placeholder="Alloted weight" name="weightAllocation" data-id={idx} />
              </td>
              <td>
                { 
                  <Button variant="danger" onClick={(() => this.props.delete(val))} > <FontAwesomeIcon icon={faMinus} size = "sm" /> </Button>
                }
              </td>
              
            </tr >
   
          </>
        )
      })
    )
  }
}
  

export default AssignJobList