import React, {PureComponent} from 'react';
import AssignJobList from "./assignJobList"
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import { faPrint } from "@fortawesome/free-solid-svg-icons";
import axios from 'axios'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 

class AssignJobForm extends PureComponent 
{
    constructor(props){
        super(props)
        this.state = {
            taskList : [{ index: Math.random(), Name: "", weightAllocation: "" }],
            add : 1,
            del : 1,
            fieldError : '',
            complete: false
        }
        this.addNewRow = this.addNewRow.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clickOnDelete = this.clickOnDelete.bind(this);
        this.validate = this.validate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }

    handleChange(e) {
        if (["Name", "weightAllocation"].includes(e.target.name)) {
           let taskList = [...this.state.taskList]
           taskList[e.target.dataset.id][e.target.name] = e.target.value
        } 
    }

    addNewRow() {
        this.setState((prevState) => ({
            taskList: [...prevState.taskList, { index: Math.random(), Name: "", Age: "" ,SeatNo:""}]
        }));   
        this.setState ({
           add: this.state.add+1
        });
    }

    validate () {
        var fieldError = " ";
        for(var i = 0; i < this.state.taskList.length; i++) {
            if(this.state.taskList[i]['weightAllocation'] == "" || this.state.taskList[i]['Name'] == "" ) {
                fieldError = '* All fields must be filled';
            }    
        } 

        if(fieldError!= " ") {
            this.setState({fieldError})
            return false;
        }
        return true;    
    }

    print()
    {
        window.print()
    }
    
    handleSubmit (e) {
        e.preventDefault();
        const isValid= this.validate();
        if(isValid) {
            this.setState({fieldError : ''})
            const obj = {
                taskList : this.state.taskList,
                user_id : this.props.id,
            }
            axios.post('http://localhost:5000/php/jobAllocation/allocate.php',obj)
            .then(response => {
               console.log(response.data)
                })
            .catch(err => console.log(err))
            this.setState({complete:true})
        }
    }

    clickOnDelete(record) {
        this.setState({
            taskList: this.state.taskList.filter(r => r !== record)
        });
        this.setState({
            del: this.state.del+1
        });
    }
   
    render() {

            let { taskList } = this.state
           
            return(   <>
                   
                    <Form onChange={this.handleChange} >
                        <Card>
                            <Card.Body>
                                <div style = {{ fontSize:12, color:"red"}}>
                                    {this.state.fieldError}
                                </div>   
                                <Table responsive>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Weight Allocation</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            <AssignJobList 
                                                add={this.addNewRow} 
                                                delete={this.clickOnDelete} 
                                                taskList={taskList} 
                                            />
                                        </tbody>
                                </Table>  
                                <Button variant="dark"  onClick={this.addNewRow} style = {{float :"right"}} > Add device </Button>      
                                      <br/><br/>  
                                {
                                    this.state.complete ?  <div style = {{float :"right"}} onClick = {this.print}>
                                        <FontAwesomeIcon icon={faPrint} size = "lg" />
                                    </div> :
                                    <Button variant = "success" 
                                    onClick = {this.handleSubmit} style = {{float :"right"}} > Apply </Button>
                                }
                                       
                                
                            </Card.Body> 
                        </Card>      
                    </Form>
                    <br/>
                    <br/>
                </>)
            
            
    }
}

export default AssignJobForm


