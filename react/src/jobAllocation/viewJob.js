import Modal from 'react-bootstrap/Modal'
import ViewJobDetails from './viewJobDetails'
import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/viewJob'

class ViewJob extends Component 
{
  constructor(props) {
    super(props)
    this.state = {
      show: true,
      count : false
    }
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.setState({ show: false, count : true});
    this.props.hideViewJob() 
    window.location.reload()
  }

render(){
    return (
        <>
          <Modal
            size="lg"
            show = {this.props.show && this.state.show}
            onHide = {this.handleClose}
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-modal-sizes-title-lg">
                Employee Name {'  '} : {this.props.name}  {' '} {'('+this.props.id+ ')'}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body><ViewJobDetails id = {this.props.id} reopen = {this.reopen}/></Modal.Body>
          </Modal>
        </>
      );
}
}

const mapStateToProps = state => {
  return {
      show : state.viewJob.show,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    hideViewJob  : () => dispatch(actionTypes.hideViewJob ()), 
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ViewJob) 
