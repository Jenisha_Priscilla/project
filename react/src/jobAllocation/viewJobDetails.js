import React, { PureComponent } from 'react'
import axios from 'axios'
import Table from 'react-bootstrap/Table'
import ViewJobList from './viewJobList'
import Card from 'react-bootstrap/Card'

class ViewJobDetails extends PureComponent{
    constructor(props) {
        super(props);
        this.state = {
            jobAllocation : []         
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/php/jobAllocation/viewJob.php/?id='+this.props.id)
        .then(response => {
            this.setState({jobAllocation : response.data})
            console.log(response.data)
        })
        
    }

    jobAllocationList() {
        
        return this.state.jobAllocation.map (function(object, i) {
            return <ViewJobList obj = {object} key ={i} />;        
      });
     
    }

    render()
    {
        return (
            <Card>
            <Card.Body>
                    <Table responsive >
                        <thead>
                            <tr>
                                <th> Device Name</th>
                                <th> Weight allocated </th>
                                <th> Weight taken </th>
                                <th> Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.jobAllocationList()}
                        </tbody>
                    </Table> 
                    </Card.Body>
                    </Card>
                        
        )
    }
}

export default ViewJobDetails