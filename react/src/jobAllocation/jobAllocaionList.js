import React, { Component } from 'react'
import ViewJob from './viewJob'
import AssignJob from './assignJob'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/viewJob'
import * as actionType from '../store/actions/assignJob'

class JobAllocationList extends Component
{
    constructor(props) {
        super(props);          
        this.state = {
            id : '',
            name : '',
            view : false,
            assign : false
        }
        this.view = this.view.bind(this);
        this.assign = this.assign.bind(this);
    }

    assign(id, name) {
        this.setState({id:id, name:name, assign:true})
        this.props.showAssignJob()
    }

    view(id, name) {
        this.setState({id:id, name:name, view:true})
        this.props.showViewJob()
    }



    render(){
        return (
            <>
            <tr>
                        <td>
                            {this.props.obj.user_id}
                        </td>
                        <td>
                            {this.props.obj.user_name}
                        </td>
                        {
                            this.props.obj.devices === null ? 
                        
                            <td >
                                 <div style = {{ color: 'green',fontWeight : 'bold'}} onClick = {() => this.assign(this.props.obj.user_id, this.props.obj.user_name)}> Assign job </div>
                                {/* <a href = { "http://localhost:3000/assignJob/"+this.props.obj.user_id+"/"+ this.props.obj.user_name } style = {{ color: 'green',fontWeight : 'bold'}}>
                                    Assign job
                                </a> */}
                            </td> :
                            <td  >
                                <div style = {{ color: 'red',fontWeight : 'bold'}} onClick = {() => this.view(this.props.obj.user_id, this.props.obj.user_name)}> View details </div>
                                {/* <a href = { "http://localhost:3000/viewJob/"+this.props.obj.user_id+"/"+ this.props.obj.user_name } style = {{ color: 'red',fontWeight : 'bold'}}>
                                    View details
                                </a> */}
                            </td>
                        }
                       
                        <td>
                            {this.props.obj.devices}
                        </td>
                        {
                            this.state.view ? <ViewJob id = {this.state.id} name = {this.state.name}/> : null
                        }
                         {
                            this.state.assign ? <AssignJob id = {this.state.id} name = {this.state.name}/> : null
                        }
                    </tr> 
                    </>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
      showViewJob  : () => dispatch(actionTypes.showViewJob ()), 
      showAssignJob : ()=> dispatch(actionType.showAssignJob ())
    }
  }
  export default connect(null, mapDispatchToProps)(JobAllocationList) 
  

