import React, { Component } from 'react'
import axios from 'axios'
import Table from 'react-bootstrap/Table'
import JobAllocationList from './jobAllocaionList'

class JobAllocation extends Component{
    constructor(props) {
        super(props);
        this.state = {
            jobAllocation : []         
        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/php/jobAllocation/jobAllocation.php')
        .then(response => {
            this.setState({jobAllocation : response.data})
            console.log(response.data)
        })
    }

    jobAllocationList() {
        return this.state.jobAllocation.map (function(object, i) {
            return <JobAllocationList obj = {object} key ={i} />;        
      });
    }

    render()
    {
      
        return (
            <div style = {{marginTop : '20px', marginLeft : '100px', marginRight: '100px'}}>
                    <Table responsive hover>
                        <thead>
                            <tr>
                                <th> Employee id</th>
                                <th> Name </th>
                                <th> Job allocation</th>
                                <th> Job summary</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.jobAllocationList()}
                        </tbody>
                    </Table> 
             
                </div>
        )
    }
}



export default JobAllocation