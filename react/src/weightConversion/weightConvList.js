import React, { PureComponent } from 'react'


class WeightConvList extends PureComponent
{
    render() {
 
        let result = ( <tr>
                        <td>
                            {this.props.obj.device_id}
                        </td>
                        <td>
                            {this.props.obj.device_name}
                        </td>
                        <td >
                            {this.props.obj.raw_weight}
                        </td>
                        <td >
                            {this.props.obj.per_piece_weight}
                        </td>
                        <td>
                            {this.props.obj.kg}
                        </td>
                        <td >
                            {this.props.obj.ounce}
                        </td>
                        <td >
                            {this.props.obj.pound}
                        </td>
                        <td>
                            {this.props.obj.qty}
                        </td>
                        <td>
                            {this.props.obj.ltrs}
                        </td>
                        
                    </tr> )

        return result;
        
    }
}

export default WeightConvList



