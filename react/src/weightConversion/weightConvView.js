import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import WeightConvList from './weightConvList'

const WeightConvView = ()=> {

    const [data, setData] = useState([])
    useEffect(() =>{
        axios.get('http://localhost:5000/php/weightConversion/weightConversion.php')
        .then(response => {
            console.log(response.data)
            setData(response.data)
        })
        .catch (function(error) {
            console.log(error);
        })
    }, [])
    
   const weigthConvList = () => {
        return data.map (function(object, i) {
            return <WeightConvList obj = {object} key ={i} />;        
      });
    }
    const style = {
        width: '95vm'
    }
        return (
            
         <div style = {{marginTop: '20px'}}>
                   
                    <Table responsive striped bordered hover style = {style}>
                    <thead>
                            <tr>
                                <th> Id</th>
                                <th> Device name </th>
                                <th> Raw weight </th>
                                <th> Per peice weight </th>
                                <th> Weight in kg </th>
                                <th> Weight in ounce </th>
                                <th> Weight in pound </th>
                                <th> Quantity </th>
                                <th> Liter </th>
                            </tr>
                        </thead>
                        <tbody>
                            {weigthConvList()}
                        </tbody>
                    </Table> 
             
                </div>
        )
    
}

export default WeightConvView

