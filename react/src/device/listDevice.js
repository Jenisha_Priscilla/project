import React, { PureComponent } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import DeviceEdit from './editDevice';
import * as actionTypes from '../store/actions/device'
class DeviceList extends PureComponent
{
    constructor(props) {
        super(props);
        this.edit = this.edit.bind(this);
        this.state = {
            redirect: false,
        }
    }
    
    edit() {
        this.setState ({
            redirect : !this.state.redirect
        })
        console.log('onedit')
        this.props.onUpdate();
    }
    
  
    
    render() {
        console.log(this.props)
        let result = ( <tr>
                        <td>
                            {this.props.obj.device_id}
                        </td>
                        {/* <td style = {{width : '300px'}}>
                            {this.props.obj.mac_id}
                        </td> */}
                        <td>
                            {this.props.obj.device_name}
                        </td>
                        <td style = {{width : '200px'}}>
                            {this.props.obj.wifi_ssid}
                        </td>
                        <td style = {{width : '200px'}}>
                            {this.props.obj.wifi_password}
                        </td>
                        <td>
                            {this.props.obj.buzz_status}
                        </td>
                        <td>
                            {this.props.obj.raw_weight}
                        </td>
                        <td>
                            {this.props.obj.tare_weight}
                        </td>
                        <td>
                            {this.props.obj.calibrate_weight}
                        </td>
                        <td>
                            {this.props.obj.known_weight}
                        </td>
                        <td>
                            {this.props.obj.max_scale_weight}
                        </td>
                        <td>
                            {this.props.obj.calculated_weight}
                        </td>
                        <td style = {{width : '300px'}}>
                            {this.props.obj.raw_weight_sync_at}
                        </td>
                        <td style = {{width : '300px'}}>
                            {this.props.obj.tare_weight_sync_at}
                        </td>
                        <td style = {{width : '300px'}}>
                            {this.props.obj.calibrate_weight_sync_at}
                        </td>
                        { this.props.update == 1  ?
                             <td style = {{width: '2px'}}>
                             <div onClick =  {()=> this.edit()}>  
                                 <FontAwesomeIcon icon = {faPen} size = "sm" />
                             </div>
                             </td> :null
                        }
                    </tr> )
                
       
        if (this.state.redirect && !this.props.updateSuccess ) {
            result =  <DeviceEdit id = {this.props.obj.device_id}  onEdit = {this.edit}/>
        }
       
       
        return result;
        
    }
}

const mapStateToProps = state => {
    return {
        update: state.device.update,
        updateSuccess : state.device.updateSuccess
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdate : () => dispatch(actionTypes.updateDevice())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeviceList)



