import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/device'
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faCheck } from "@fortawesome/free-solid-svg-icons";

class DeviceEdit extends Component 
{
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            fieldError : ''
        }
    }

    componentDidMount() {
        console.log(this.props.id)
        this.props.getDevice(this.props.id);
    }

    validate() {
        var fieldError = " ";
       
        
        if (this.props.device.wifi_ssid == '' || this.props.device.wifi_password == '' ||
        this.props.device.calibrate_weight == '' || this.props.device.known_weight == ''||
        this.props.device.max_scale_weight == '' ) {
            fieldError = 'All fields must be filled';
        }
        if (fieldError != " "  ) {
            this.setState({fieldError})
            return false;
        }
        return true;
    }
   
    onSubmit(e) {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const obj = {
                id:this.props.id,
                wifi_ssid:this.props.device.wifi_ssid,
                wifi_password:this.props.device.wifi_password,
                calibrate_weight:this.props.device.calibrate_weight,
                known_weight:this.props.device.known_weight,
                max_scale_weight:this.props.device.max_scale_weight
            };

            console.log(obj)
            axios.post('http://localhost:5000/php/device/updateDevice.php',obj)
            .then(response => {
                  console.log(response.data)
               
                    alert('Data edited successfully');
                   this.props.onEdit()
                   this.props.deviceUpdateSuccess();
                
            })
            .catch(err => console.log(err))
        }
    }
    render() {
        console.log("edit")
        return (
            <tr >    
                <td>
                    {this.props.device.device_id}
                </td>
                {/* <td>
                    {this.props.device.mac_id}
                </td> */}
                <td>
                    {this.props.device.device_name}
                </td>  
                <td style = {{width : '350px'}}> 
                    <Form.Control type = "text" name = "wifi_ssid"
                        value = {this.props.device.wifi_ssid}
                        onChange = {this.props.onDeviceChange}
                    />
                </td>
                <td style = {{width : '350px'}}>
                    <Form.Control type = "text" name = "wifi_password"
                        value = {this.props.device.wifi_password}
                        onChange = {this.props.onDeviceChange}
                    />
                </td>
                <td>
                   {this.props.device.buzz_status}
                </td>
                <td>
                    {this.props.device.raw_weight}
                </td>
                <td>
                    {this.props.device.tare_weight}
                </td>
                <td >
                    <Form.Control type = "text" name = "calibrate_weight"
                        value = {this.props.device.calibrate_weight}
                        onChange = {this.props.onDeviceChange}
                    />
                </td>
                <td >
                    <Form.Control type = "text" name = "known_weight"
                        value = {this.props.device.known_weight}
                        onChange = {this.props.onDeviceChange}
                    />
                </td>
                <td >
                    <Form.Control type = "text" name = "max_scale_weight"
                        value = {this.props.device.max_scale_weight}
                        onChange = {this.props.onDeviceChange}
                    />
                </td>
                <td>
                    {this.props.device.calculated_weight}
                </td>
                <td>
                    {this.props.device.raw_weight_sync_at}
                </td>
                <td>
                    {this.props.device.tare_weight_sync_at}
                </td>
                <td>
                    {this.props.device.calibrate_weight_sync_at}
                </td>
                <td > 
                    <div variant="dark" onClick = {this.onSubmit} > 
                        <FontAwesomeIcon icon = {faCheck} size = "sm" />
                    </div>
                </td>
            </tr>      
            )
        }  
    }
    const mapStateToProps = state => {
        return {
            device : state.device.device,
        }
    }
    
    const mapDispatchToProps = dispatch => {
        return {
            getDevice : (deviceId) => dispatch(actionTypes.getDevice(deviceId)), 
            onDeviceChange : (e) => dispatch(actionTypes.handleDeviceEdit(e)),
            deviceUpdateSuccess : () => dispatch(actionTypes.deviceUpdateSuccess())
        }
    }

export default connect(mapStateToProps, mapDispatchToProps)(DeviceEdit) 