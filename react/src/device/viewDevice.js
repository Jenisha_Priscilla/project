import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import DeviceList from './listDevice'
import { connect } from 'react-redux';

const DeviceView  = (props)=> {

    const [deviceData, setDeviceData] = useState([])
    useEffect(() =>{
        axios.get('http://localhost:5000/php/device/viewDevice.php')
        .then(response => {
            setDeviceData(response.data)
        })
        .catch (function(error) {
            console.log(error);
        })
    }, [ props.updateSuccess])
    
   const deviceList = () => {
        return deviceData.map (function(object, i) {
            return <DeviceList obj = {object} key ={i} />;        
      });
    }
    const style = {
        width: '95vm'
    }
        return (
            
         <div style = {{marginTop: '20px'}}>
                   
                    <Table responsive striped bordered hover style = {style}>
                        <thead>
                        <tr>
                            
                            <th colSpan ="4"><center> Device</center> </th>
                            <th></th>
                            <th colSpan ="6"> <center> Weight</center></th>
                            <th colSpan ="3"><center> Last commincation at </center></th>
                            <th>Action</th>
                           
                        </tr>
                            <tr>
                                <th> Id</th>
                                {/* <th> MAC id</th> */}
                                <th> Name </th>
                                <th> SSID</th>
                                <th> Password</th>
                                <th> Buzz</th>
                                <th> Raw wt </th>
                                <th> Tare wt</th>
                                <th> Calibrate wt</th>
                                <th> Known wt</th>
                                <th> Max scale wt</th>
                                <th> Cal wt </th>
                                <th> raw wt sync at</th>
                                <th> tare wt sync at</th>
                                <th> calibrate wt sync at</th>
                                <th > Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            {deviceList()}
                        </tbody>
                    </Table> 
             
                </div>
        )
    
}
const mapStateToProps = state => {

    return {
      
        updateSuccess : state.device.updateSuccess
    }
}
export default connect(mapStateToProps, null)(DeviceView)

