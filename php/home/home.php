<?php
include "../connect.php";
$sql = "select * from
(select distinct a.user_id as job_user, a.job_createdon , c.device_id, a.job_status from joballocation as a right join
(select devices.device_id, min(joballocation.job_createdon) as job,
devices.buzz_status from joballocation right join devices 
on devices.device_id = joballocation.device_id 
group by devices.device_id)as c on a.device_id = c.device_id and c.job = a.job_createdon)as jo inner join
(select devices.device_id, devices.device_name, devices.raw_weight,refill.user_id as refill_user, refill.refill_createdon,refill.refill_status,
devices.buzz_status from refill right join devices 
on devices.device_id = refill.device_id order by devices.device_id)as ref on jo.device_id= ref.device_id order by ref.device_id;";
$query = $pdo->query($sql);
$cr = 0;
while ($row = $query->fetch()) {
    $data[$cr]['device_id'] = $row['device_id'];	
    $data[$cr]['device_name'] = $row['device_name'];	
    $data[$cr]['raw_weight'] = $row['raw_weight'];	
    $data[$cr]['job_user'] = $row['job_user'];	
    $data[$cr]['job_createdon'] = $row['job_createdon'];	
    $data[$cr]['job_status'] = $row['job_status'];	
    $data[$cr]['refill_user'] = $row['refill_user'];	
    $data[$cr]['refill_createdon'] = $row['refill_createdon'];	
    $data[$cr]['refill_status'] = $row['refill_status'];	
    $data[$cr]['buzz_status'] = $row['buzz_status'];
    $cr++;
}

print json_encode($data);
