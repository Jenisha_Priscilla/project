<?php
include "../connect.php";
$sql = "select device_id, device_name, per_piece_weight, raw_weight, per_piece_weight/1000 as kg, per_piece_weight/28.35 as ounce,
per_piece_weight/454 as pound, round(raw_weight/per_piece_weight) as qty, 
if(device_name = 'water',per_piece_weight/1000,if(device_name = 'oil',per_piece_weight/910, null)) as ltrs
from devices;";
$query = $pdo->query($sql);
$cr = 0;
	while ($row = $query->fetch()) {
        $data[$cr]['device_id'] = $row['device_id'];		
        $data[$cr]['device_name'] = $row['device_name'];
        $data[$cr]['raw_weight'] = $row['raw_weight'];
        $data[$cr]['per_piece_weight'] = $row['per_piece_weight'];
        $data[$cr]['kg'] = $row['kg'];
        $data[$cr]['ounce'] = $row['ounce'];
        $data[$cr]['pound'] = $row['pound'];
        $data[$cr]['qty'] = $row['qty'];
        $data[$cr]['ltrs'] = $row['ltrs'];
        $cr++;
    }
   
   print json_encode( $data);
