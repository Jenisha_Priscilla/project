<?php
include "../connect.php";

$sql = "select refill.refill_id, refill.user_id, devices.device_id,refill.refill_createdon,refill.refill_updatedon ,devices.raw_weight, users.user_name ,refill.refill_weight, devices.device_name, refill.refill_status from devices left join refill on devices.device_id = refill.device_id
left join users on users.user_id = refill.user_id;";
$query = $pdo->query($sql);
$data = [];
$cr = 0;
while ($row = $query->fetch()) {
    $data[$cr]['refill_id'] = $row['refill_id'];
    $data[$cr]['user_id'] = $row['user_id'];
    $data[$cr]['user_name'] = $row['user_name'];
    $data[$cr]['refill_weight'] = $row['refill_weight'];
    $data[$cr]['device_name'] = $row['device_name'];
    $data[$cr]['device_id'] = $row['device_id'];
    $data[$cr]['refill_status'] = $row['refill_status'];
    $data[$cr]['refill_createdon'] = $row['refill_createdon'];
    $data[$cr]['refill_updatedon'] = $row['refill_updatedon'];
    $data[$cr]['raw_weight'] = $row['raw_weight'];
    $cr++;	
}

print json_encode($data);