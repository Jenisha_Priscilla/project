<?php
include "../connect.php";
$id =$_GET['id'];
$sql = "SELECT * FROM devices where device_id = '$id'";
$query = $pdo->query($sql);
$device = [];
$cr = 0;
while ($row = $query->fetch()) {
    $device[$cr]['device_id'] = $row['device_id'];
    $device[$cr]['mac_id'] = $row['mac_id'];
    $device[$cr]['device_name'] = $row['device_name'];
    $device[$cr]['wifi_ssid'] = $row['wifi_ssid'];
    $device[$cr]['wifi_password'] = $row['wifi_password'];
    $device[$cr]['buzz_status'] = $row['buzz_status'];
    $device[$cr]['raw_weight'] = $row['raw_weight'];
    $device[$cr]['raw_weight_sync_at'] = $row['raw_weight_sync_at'];
    $device[$cr]['tare_weight'] = $row['tare_weight'];
    $device[$cr]['tare_weight_sync_at'] = $row['tare_weight_sync_at'];
    $device[$cr]['calibrate_weight'] = $row['calibrate_weight'];
    $device[$cr]['calibrate_weight_sync_at'] = $row['calibrate_weight_sync_at'];
    $device[$cr]['known_weight'] = $row['known_weight'];
    $device[$cr]['max_scale_weight'] = $row['max_scale_weight'];
    $device[$cr]['calculated_weight'] = $row['calculated_weight'];
    $cr++;	
}

print json_encode($device);