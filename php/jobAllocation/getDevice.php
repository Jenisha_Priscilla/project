<?php
include "../connect.php";

$sql = "select devices.device_name,devices.device_id from devices ;";
$query = $pdo->query($sql);
$device = [];
$cr = 0;
while ($row = $query->fetch()) {
    $device[$cr]['device_id'] = $row['device_id'];
    $device[$cr]['device_name'] = $row['device_name'];
    $cr++;	
}

print json_encode($device);