use work;

CREATE TABLE Employee (
id INT AUTO_INCREMENT PRIMARY KEY,
Employeename VARCHAR(30) ,
Gender VARCHAR(30) ,
Role VARCHAR(30) ,
DOB date ,
Qualification varchar(20),
ContactNo int,
RFId varchar(30),
email VARCHAR(50),
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
