-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: scaleapp
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `mac_id` varchar(150) NOT NULL,
  `device_name` varchar(150) DEFAULT NULL,
  `wifi_ssid` varchar(150) DEFAULT NULL,
  `wifi_password` varchar(150) DEFAULT NULL,
  `buzz_status` tinyint(1) NOT NULL DEFAULT '0',
  `raw_weight` int(11) NOT NULL DEFAULT '0',
  `raw_weight_sync_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `tare_weight` int(11) NOT NULL DEFAULT '0',
  `tare_weight_sync_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `calibrate_weight` int(11) NOT NULL DEFAULT '0',
  `calibrate_weight_sync_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `known_weight` int(11) NOT NULL DEFAULT '0',
  `max_scale_weight` int(11) NOT NULL DEFAULT '0',
  `calculated_weight` decimal(10,3) NOT NULL DEFAULT '0.000',
  `createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedon` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `per_piece_weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  UNIQUE KEY `mac_id_UNIQUE` (`mac_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'9C-35-5B-5F-4C-D7','sugar','scalewifi','scaleapp',0,170,'2020-08-29 11:44:06',0,'2020-08-29 11:44:06',0,'2020-08-29 11:44:06',0,0,0.000,'2020-08-29 11:44:06','2020-09-15 00:47:08',17),(2,'8C-45-5B-5F-4C-D7','wheat','scalewifi','scaleapp',0,100,'2020-09-03 11:00:30',0,'2020-09-03 11:00:30',0,'2020-09-03 11:00:30',0,0,0.000,'2020-09-03 11:00:30','2020-09-15 00:47:15',20),(3,'7C-45-5B-5F-4C-D7','rice','scalewifi','scaleapp',0,100,'2020-09-03 11:35:10',0,'2020-09-03 11:35:10',0,'2020-09-03 11:35:10',0,0,0.000,'2020-09-03 11:35:10','2020-09-15 00:47:18',20),(4,'3C-45-5B-5F-4C-D7','chocolate','scalewifi','scaleapp',0,100,'2020-09-03 14:15:55',0,'2020-09-03 14:15:55',0,'2020-09-03 14:15:55',0,0,0.000,'2020-09-03 14:15:55','2020-09-15 00:47:20',25),(5,'2C-45-5B-5F-4C-D7','icecream','scalewifi','scaleapp',0,100,'2020-09-03 15:35:32',0,'2020-09-03 15:35:32',0,'2020-09-03 15:35:32',0,0,0.000,'2020-09-03 15:35:32','2020-09-15 00:47:23',25),(6,'4D-45-5B-5F-4C-D7','waffer','scalewifi','scaleapp',0,300,'2020-09-03 16:47:59',0,'2020-09-03 16:47:59',0,'2020-09-03 16:47:59',0,0,0.000,'2020-09-03 16:47:59','2020-09-15 00:47:31',50),(8,'5D-45-5B-5F-4C-D7','water','scalewifi','scaleapp',0,300,'2020-09-03 16:51:16',0,'2020-09-03 16:51:16',0,'2020-09-03 16:51:16',0,0,0.000,'2020-09-03 16:51:16','2020-09-15 00:47:38',20),(9,'7D-45-5B-5F-4C-D7','soap','scalewifi','scaleapp',0,100,'2020-09-03 17:41:17',0,'2020-09-03 17:41:17',0,'2020-09-03 17:41:17',0,0,0.000,'2020-09-03 17:41:17','2020-09-15 00:47:43',20),(10,'9D-45-5B-5F-4C-D7','shampoo','scalewifi','scaleapp',0,100,'2020-09-03 17:41:26',0,'2020-09-03 17:41:26',0,'2020-09-03 17:41:26',0,0,0.000,'2020-09-03 17:41:26','2020-09-15 00:47:47',30),(11,'3D-45-5B-5F-4C-D7','honey','scalewifi','scaleapp',0,100,'2020-09-03 18:31:45',0,'2020-09-03 18:31:45',0,'2020-09-03 18:31:45',0,0,0.000,'2020-09-03 18:31:45','2020-09-15 00:47:51',30),(12,'7G-45-5B-5F-4C-D7','Lays','scalewifi','scaleapp',0,100,'2020-09-10 15:53:48',0,'2020-09-10 15:53:48',0,'2020-09-10 15:53:48',0,0,0.000,'2020-09-10 15:53:48','2020-09-15 00:47:53',30);
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joballocation`
--

DROP TABLE IF EXISTS `joballocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joballocation` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `weight_allocated` varchar(45) NOT NULL,
  `weight_taken` varchar(45) DEFAULT NULL,
  `job_status` tinyint(1) NOT NULL DEFAULT '0',
  `job_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `job_updatedon` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`job_id`,`device_id`,`user_id`),
  KEY `fk_joballocation_devices1_idx` (`device_id`),
  KEY `fk_joballocation_users1_idx` (`user_id`),
  CONSTRAINT `fk_joballocation_devices1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_joballocation_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joballocation`
--

LOCK TABLES `joballocation` WRITE;
/*!40000 ALTER TABLE `joballocation` DISABLE KEYS */;
INSERT INTO `joballocation` VALUES (1,1,1,'250g',NULL,0,'2020-09-03 10:59:56',NULL),(2,2,1,'250g',NULL,0,'2020-09-03 11:00:59',NULL),(3,3,1,'250g',NULL,0,'2020-09-03 11:35:26','2020-09-03 11:38:15'),(4,4,15,'23',NULL,0,'2020-09-03 15:52:40',NULL),(5,5,15,'22',NULL,0,'2020-09-03 15:52:41',NULL),(6,6,16,'2',NULL,0,'2020-09-03 16:48:09',NULL),(7,8,18,'3',NULL,0,'2020-09-03 16:51:38',NULL),(8,9,17,'5',NULL,0,'2020-09-03 17:42:50',NULL),(9,10,19,'6',NULL,0,'2020-09-03 17:43:02',NULL),(11,1,20,'10',NULL,0,'2020-09-10 03:00:46',NULL),(12,2,20,'10',NULL,0,'2020-09-10 03:00:46',NULL);
/*!40000 ALTER TABLE `joballocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refill`
--

DROP TABLE IF EXISTS `refill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refill` (
  `refill_id` int(11) NOT NULL AUTO_INCREMENT,
  `refill_weight` decimal(10,2) NOT NULL,
  `device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `refill_status` tinyint(1) NOT NULL DEFAULT '0',
  `refill_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `refill_updatedon` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`refill_id`,`device_id`,`user_id`),
  KEY `fk_refill_devices1_idx` (`device_id`),
  KEY `fk_refill_users1_idx` (`user_id`),
  CONSTRAINT `fk_refill_devices1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_refill_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refill`
--

LOCK TABLES `refill` WRITE;
/*!40000 ALTER TABLE `refill` DISABLE KEYS */;
INSERT INTO `refill` VALUES (1,10.00,2,20,0,'2020-09-05 12:45:41','2020-09-07 11:34:51'),(2,10.00,1,15,0,'2020-09-07 10:57:31',NULL),(3,9.00,3,16,0,'2020-09-07 11:32:09','2020-09-07 12:36:20'),(5,8.00,4,18,0,'2020-09-07 12:13:49','2020-09-07 12:52:27'),(6,8.00,5,17,0,'2020-09-07 14:03:51',NULL),(7,10.00,6,19,0,'2020-09-07 14:04:05','2020-09-15 01:25:47');
/*!40000 ALTER TABLE `refill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tare`
--

DROP TABLE IF EXISTS `tare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tare` (
  `tare_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `tare_status` tinyint(1) NOT NULL DEFAULT '0',
  `tare_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `tare_updateon` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tare_id`,`user_id`,`device_id`),
  KEY `fk_Tare_users1_idx` (`user_id`),
  KEY `fk_Tare_devices1_idx` (`device_id`),
  CONSTRAINT `fk_Tare_devices1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tare_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tare`
--

LOCK TABLES `tare` WRITE;
/*!40000 ALTER TABLE `tare` DISABLE KEYS */;
/*!40000 ALTER TABLE `tare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(150) NOT NULL,
  `role_description` varchar(150) DEFAULT NULL,
  `role_isdeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedon` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'admin','all previleges',0,'2020-08-25 10:23:36',NULL),(2,'superVisor','Monitor employee',0,'2020-08-25 10:24:29',NULL),(3,'employee','assist',0,'2020-09-03 12:43:42',NULL);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) NOT NULL,
  `user_rfid` varchar(200) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `user_pwd` varchar(150) NOT NULL,
  `user_isoccupied` tinyint(1) DEFAULT '0',
  `role_id` int(11) NOT NULL,
  `user_isdeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedon` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `fk_users_user_role_idx` (`role_id`),
  CONSTRAINT `fk_users_user_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Jenisha','12348','lovishaprincy@gmail.com','1urx6kud',0,2,0,'2020-08-25 10:40:15','2020-09-07 11:30:33'),(15,'Lovisha','2345','jebacont@gmail.com','8e886npx',0,3,0,'2020-09-03 12:43:47',NULL),(16,'Jebaraj','12354','jebacont@gmail.com','7vyhavub',0,3,0,'2020-09-03 16:47:01','2020-09-07 11:30:18'),(17,'Beaula','123','lovishaprincy@gmail.com','kr5ni1em',0,3,0,'2020-09-03 16:50:11','2020-09-07 11:30:50'),(18,'Rajan','1234','lovishaprincy@gmail.com','5ppdky0g',0,3,0,'2020-09-03 16:50:27','2020-09-07 11:31:04'),(19,'Lee','1234','lovishaprincy@gmail.com','1esbab1s',0,3,0,'2020-09-03 17:41:41','2020-09-07 11:31:13'),(20,'Renin','1234','lovishaprincy@gmail.com','goyavee9',0,3,0,'2020-09-03 17:45:57','2020-09-07 11:31:22'),(21,'Priscilla','1234','lovishaprincy@gmail.com','s7nwppaq',0,3,0,'2020-09-10 16:45:27',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weightconversion`
--

DROP TABLE IF EXISTS `weightconversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weightconversion` (
  `wc_id` int(11) NOT NULL AUTO_INCREMENT,
  `weight_perpiece` decimal(10,2) DEFAULT NULL,
  `weight_ingram` decimal(5,2) DEFAULT NULL,
  `kg` decimal(10,2) DEFAULT NULL,
  `pounds` decimal(10,2) DEFAULT NULL,
  `ounce` decimal(10,2) DEFAULT NULL,
  `lites` decimal(10,2) DEFAULT NULL,
  `quantity` decimal(10,2) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `lastupdatedon` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`wc_id`,`device_id`),
  UNIQUE KEY `wc_id_UNIQUE` (`wc_id`),
  KEY `fk_weightconversion_devices_idx` (`device_id`),
  CONSTRAINT `fk_weightconversion_devices` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weightconversion`
--

LOCK TABLES `weightconversion` WRITE;
/*!40000 ALTER TABLE `weightconversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `weightconversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'scaleapp'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-15  8:54:17
